import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TeamService {

  constructor(
    private http: HttpClient
  ) { }

  get(teamID: string): Observable<any> | undefined {
    if (!teamID) { return; }
    return this.http.get(`${environment.apiUrl}/v2/teams/${teamID}?plan=TIER_ONE`);
  }

}
