/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { TeamService } from './team.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { of } from 'rxjs';
import { TeamResultData } from './../models/team.model';


describe('Service: Team', () => {

  let teamService: TeamService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
      ],
      providers: [TeamService]
    });
  });

  beforeEach(() => {
    teamService = TestBed.inject(TeamService);
  });

  it('should return teams', inject([TeamService], (service: TeamService) => {
    const teamID = '61';
    const result = new TeamResultData();
    const spy = spyOn(teamService, 'get').and.returnValue(of(result))
    service.get(teamID);
    expect(spy).toHaveBeenCalled();
  }));

  it('should not return teams', inject([TeamService], (service: any) => {
    const teamID = undefined;
    const result = null;
    const spy = spyOn(teamService, 'get').and.returnValue(of(result))
    service.get(teamID);
    expect(result).toEqual(null);
  }));
});
