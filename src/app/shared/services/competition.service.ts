import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CompetitionService {

  constructor(
    private http: HttpClient
  ) { }

  get(query?: string): Observable<any> {
    return this.http.get(`${environment.apiUrl}/v2/competitions?plan=TIER_ONE`);
  }  

  getTeamsFromCompetition(competitionId: string): Observable<any> {
    return this.http.get(`${environment.apiUrl}/v2/competitions/${competitionId}/teams`);
  }

}
