import { Season } from "./season.model";

export class Competition {
        id!: number;
        area: any;
        name!: string;
        code!: string;
        plan!: string;
        currentSeason!: Season;
        seasons!: Array<Season>;
        lastUpdated!: string;   
}

export class CompetitionResultData {
        competitions: Array<Competition> = [];
        count!: number;
        filter: any;

        constructor() {}
}
