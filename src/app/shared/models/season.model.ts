export class Season {
    public id!: number;
    startDate!: string;
    endDate!: string;
    currentMatchday!: number;
}
