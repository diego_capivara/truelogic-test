import { Competition } from "./competition.model";
import { Season } from "./season.model";

export class Team {
    id!: number;
    area!: any;
    name!: string;
    shortName!: string;
    tla!: string;
    address!: string;
    phone!: string | number;
    website!: string;
    email!: string;
    founded!: number;
    clubColors!: string;
    venue!: string | number;
    squad!: any[];
    lastUpdated!: Date;
}

export class TeamResultData {
    competition!: Competition;
    count!: number;
    filters: any = {};
    season!: Season;
    teams!: Team[];
}