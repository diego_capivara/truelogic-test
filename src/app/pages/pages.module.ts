import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PagesComponent } from './pages.component';
import { PagesRoutes } from './pages.routing';
import { CoreModule } from '../core/core.module';

@NgModule({
  imports: [
    CommonModule,
    PagesRoutes,
    CoreModule,
  ],
  declarations: [PagesComponent]
})
export class PagesModule { }
