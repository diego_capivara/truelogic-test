import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { Team } from 'src/app/shared/models/team.model';
import { TeamService } from 'src/app/shared/services/team.service';

@Component({
  selector: 'app-players-list',
  templateUrl: './players-list.component.html',
  styleUrls: ['./players-list.component.scss']
})
export class PlayersListComponent implements OnInit {

  params: any = {};

  teamAsync!: Observable<Team> | undefined;

  columns: any[] = [
    { key: 'name', title: 'Name' },
    { key: 'position', title: 'Position' },
    { key: 'nationality', title: 'Nationality' },
  ];

  constructor(
    private route: ActivatedRoute,
    private teamService: TeamService,
  ) { }

  ngOnInit() {
    this.params = this.route.snapshot.params;
    const teamID = this.params.teamID;

    this.teamAsync = this.teamService.get(teamID);
  }

}
