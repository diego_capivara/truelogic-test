import { Routes, RouterModule } from '@angular/router';
import { PlayersListComponent } from './players-list.component';

const routes: Routes = [
  { 
    path: ':teamID',
    component: PlayersListComponent
  },
];

export const PlayersListRoutes = RouterModule.forChild(routes);
