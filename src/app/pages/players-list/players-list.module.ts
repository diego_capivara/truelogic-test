import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PlayersListComponent } from './players-list.component';
import { TeamService } from 'src/app/shared/services/team.service';
import { TableModule } from 'ngx-easy-table';
import { PlayersListRoutes } from './players-list.routing';
import { MatCardModule } from '@angular/material/card';

@NgModule({
  imports: [
    CommonModule,
    TableModule,
    PlayersListRoutes,
    MatCardModule,
  ],
  declarations: [PlayersListComponent],
  providers: [TeamService],
  exports: [PlayersListComponent],
})
export class PlayersListModule { }
