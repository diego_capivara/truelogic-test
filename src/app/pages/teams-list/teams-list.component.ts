import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { Router, ActivatedRoute } from '@angular/router';
import { Config, DefaultConfig } from 'ngx-easy-table';
import { map } from 'rxjs/operators';
import { Team } from 'src/app/shared/models/team.model';
import { CompetitionService } from 'src/app/shared/services/competition.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-teams-list',
  templateUrl: './teams-list.component.html',
  styleUrls: ['./teams-list.component.scss']
})
export class TeamsListComponent implements OnInit {

  @ViewChild(MatPaginator) paginator!: MatPaginator;

  columns: any[] = [
    { key: 'name', title: 'Name' },
    { key: 'email', title: 'Email' },
  ];
  dataSource: any = [];

  form!: FormGroup;
  params: any = {};

  public configuration!: Config;


  constructor(
    private competitionService: CompetitionService,
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.configuration = { ...DefaultConfig };

    this.params = this.route.snapshot.params;
    this.form = this.fb.group({
      startDate: [null],
    });
    this.searchTeams(this.params.competitionID);
  }

  searchTeams(competitionID: string) {
    this.competitionService.getTeamsFromCompetition(competitionID).pipe(
      map((result: any) => result.teams), 
    ).subscribe((result: Array<Team>) => {
      this.dataSource = result;
      this.dataSource.paginator = this.paginator;
    }, (err) => {
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: err.error.message,
      }).then(() => this.router.navigate(['competitions-list']));
    });
  }

  onClick(row: any) {
    const team: Team = row.value.row;
    if (team && team.id) {
      this.router.navigate([`players-list/${team.id}`]);
    }
  }

}
