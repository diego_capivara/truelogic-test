import { CommonModule, DatePipe } from '@angular/common';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatNativeDateModule } from '@angular/material/core';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTableModule } from '@angular/material/table';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ActivatedRoute, Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { TableModule } from 'ngx-easy-table';
import { of } from 'rxjs';
import { CompetitionService } from 'src/app/shared/services/competition.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TeamsListComponent } from './teams-list.component';
import { TeamsListRoutes } from './teams-list.routing';
import { Team, TeamResultData } from 'src/app/shared/models/team.model';

describe('TeamsListComponent', () => {

    let component: TeamsListComponent;
    let fixture: ComponentFixture<TeamsListComponent>;
    let competitionService: CompetitionService;
    let router: Router;
    let route: ActivatedRoute;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            imports: [
                CommonModule,
                TeamsListRoutes,
                MatDatepickerModule,
                MatNativeDateModule,
                MatFormFieldModule,
                MatInputModule,
                ReactiveFormsModule,
                HttpClientTestingModule,
                MatTableModule,
                RouterTestingModule,
                MatPaginatorModule,
                BrowserAnimationsModule,
                TableModule,
                MatButtonModule,
            ],
            providers: [CompetitionService, DatePipe],
            declarations: [TeamsListComponent]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(TeamsListComponent);
        component = fixture.componentInstance;
        competitionService = TestBed.inject(CompetitionService);
        router = TestBed.inject(Router);
        route = TestBed.inject(ActivatedRoute);
        spyOnProperty(router, 'url', 'get').and.returnValue('/competitions-list');
        fixture.detectChanges();
    });

    it('should get all teams', () => {
        const result: TeamResultData = new TeamResultData();
        result.teams = [
            new Team(),
            new Team(),
            new Team(),
        ];
        const competitionID = '2021';
        const spy = spyOn(competitionService, 'getTeamsFromCompetition').and.returnValue(of(result));
        component.searchTeams(competitionID);
        expect(spy).toHaveBeenCalled();   
        expect(component.dataSource).toEqual(result.teams);
    });

    it('should navigate to player list page success', () => {
        const competition: Team = new Team();
        competition.id = 2021;
        let row: any = { value: { row: {} } };
        row.value.row = competition;
        const spy = spyOn(router, 'navigate');
        component.onClick(row);
        expect(spy).toHaveBeenCalledWith([`players-list/${competition.id}`]);
        expect(spy).toHaveBeenCalledTimes(1);

    });

    it('should not navigate to player list page (fail)', () => {
        const competition: any = new Team();
        competition.id = null;
        let row: any = { value: { row: {} } };
        row.value.row = competition;
        const spy = spyOn(router, 'navigate');
        component.onClick(row);
        expect(spy).toHaveBeenCalledTimes(0);
    });

});
