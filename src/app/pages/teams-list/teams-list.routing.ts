import { Routes, RouterModule } from '@angular/router';
import { TeamsListComponent } from './teams-list.component';

const routes: Routes = [
  { 
    path: ':competitionID',
    component: TeamsListComponent,
  },
];

export const TeamsListRoutes = RouterModule.forChild(routes);
