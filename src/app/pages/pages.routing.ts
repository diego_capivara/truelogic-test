import { Routes, RouterModule } from '@angular/router';
import { PagesComponent } from './pages.component';

const routes: Routes = [
  {  
    path: '',
    component: PagesComponent,
    children: [
      {
        path: 'competitions-list',
        loadChildren: () => import('./competitions-list/competitions-list.module').then(m => m.CompetitionsListModule),
      },
      {
        path: 'teams-list',
        loadChildren: () => import('./teams-list/teams-list.module').then(m => m.TeamsListModule),
      },
      {
        path: 'players-list',
        loadChildren: () => import('./players-list/players-list.module').then(m => m.PlayersListModule),
      },
      {
        path: '**',
        redirectTo: 'competitions-list',
        pathMatch: 'full'
      }
    ]
  },
];

export const PagesRoutes = RouterModule.forChild(routes);
