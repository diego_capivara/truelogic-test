import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { CompetitionsListComponent } from './competitions-list.component';
import { CompetitionsListRoutes } from './competitions-list.routing';
import { CompetitionService } from 'src/app/shared/services/competition.service';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { ReactiveFormsModule } from '@angular/forms';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { TableModule } from 'ngx-easy-table';
import { MatButtonModule } from '@angular/material/button';

@NgModule({
  imports: [
    CommonModule,
    CompetitionsListRoutes,
    MatDatepickerModule,
    MatNativeDateModule,
    MatFormFieldModule,
    MatInputModule,
    ReactiveFormsModule,
    MatTableModule,
    MatPaginatorModule,
    TableModule,
    MatButtonModule,
  ],
  providers: [CompetitionService, DatePipe],
  declarations: [CompetitionsListComponent]
})
export class CompetitionsListModule { }
