import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Competition, CompetitionResultData } from 'src/app/shared/models/competition.model';
import { CompetitionService } from 'src/app/shared/services/competition.service';
import { map } from 'rxjs/operators';
import { MatPaginator } from '@angular/material/paginator';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-competitions-list',
  templateUrl: './competitions-list.component.html',
  styleUrls: ['./competitions-list.component.scss']
})
export class CompetitionsListComponent implements OnInit {

  @ViewChild(MatPaginator) paginator!: MatPaginator;

  columns: any[] = [
    { key: 'id', title: 'id' },
    { key: 'name', title: 'Name' },
    { key: 'plan', title: 'Plan' },
  ];
  dataSource: any = [];

  form!: FormGroup;

  constructor(
    private competitionService: CompetitionService,
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.form = this.fb.group({
      startDate: [null],
    });

    this.route.queryParams.subscribe(queryParams => {
      queryParams && queryParams.season ? this.filterCompetitionBySeason(queryParams.season) : this.searchCompetitions();
    });

    this.form.get('startDate')?.valueChanges.subscribe(value => {
      const dateFormatted = Date.parse(value);
      this.router.navigate(
        [],
        {
          relativeTo: this.route,
          queryParams: { season: dateFormatted },
          queryParamsHandling: 'merge'
        });
    });
  }

  clear() {
    this.router.navigate(
      [],
      {
        relativeTo: this.route,
        queryParams: { season: null },
        queryParamsHandling: 'merge'
      });
  }

  searchCompetitions() {
    this.competitionService.get().pipe(
      map((result: CompetitionResultData) => result.competitions),
    ).subscribe((result: Array<Competition>) => {
      this.dataSource = result;
      this.dataSource.paginator = this.paginator;
    });
  }

  filterCompetitionBySeason(date: any) {
    this.competitionService.get().pipe(
      map((result: CompetitionResultData) => {
        const competitions = result.competitions;
        return competitions.filter((item: Competition) => {
          if (item.currentSeason && item.currentSeason.startDate) {
            const formattedDate = Date.parse(item.currentSeason.startDate);
            return date <= formattedDate;
          }
          return;
        })
      })
    ).subscribe((result: Array<Competition>) => {
      this.dataSource = result;
      this.dataSource.paginator = this.paginator;
    });
  }

  onClick(row: any) {
    const competition: Competition = row.value.row;
    if (competition && competition.id) {
      this.router.navigate([`teams-list/${competition.id}`]);
    }
  }
}
