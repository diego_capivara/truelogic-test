import { Routes, RouterModule } from '@angular/router';
import { CompetitionsListComponent } from './competitions-list.component';

const routes: Routes = [
  {  
    path: '',
    component: CompetitionsListComponent,
  },
];

export const CompetitionsListRoutes = RouterModule.forChild(routes);
