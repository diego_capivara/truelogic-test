import { CommonModule, DatePipe } from '@angular/common';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatNativeDateModule } from '@angular/material/core';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTableModule } from '@angular/material/table';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ActivatedRoute, Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { TableModule } from 'ngx-easy-table';
import { of } from 'rxjs';
import { CompetitionService } from 'src/app/shared/services/competition.service';
import { CompetitionsListComponent } from './competitions-list.component';
import { CompetitionsListRoutes } from './competitions-list.routing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { Competition, CompetitionResultData } from 'src/app/shared/models/competition.model';

describe('CompetitionsListComponent', () => {

    let component: CompetitionsListComponent;
    let fixture: ComponentFixture<CompetitionsListComponent>;
    let competitionService: CompetitionService;
    let router: Router;
    let route: ActivatedRoute;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            imports: [
                CommonModule,
                CompetitionsListRoutes,
                MatDatepickerModule,
                MatNativeDateModule,
                MatFormFieldModule,
                MatInputModule,
                ReactiveFormsModule,
                HttpClientTestingModule,
                MatTableModule,
                RouterTestingModule,
                MatPaginatorModule,
                BrowserAnimationsModule,
                TableModule,
                MatButtonModule,
            ],
            providers: [CompetitionService, DatePipe],
            declarations: [CompetitionsListComponent]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(CompetitionsListComponent);
        component = fixture.componentInstance;
        competitionService = TestBed.inject(CompetitionService);
        router = TestBed.inject(Router);
        route = TestBed.inject(ActivatedRoute);
        spyOnProperty(router, 'url', 'get').and.returnValue('/competitions-list');
        fixture.detectChanges();
    });

    it('should clear filter', () => {
        const spy = spyOn(router, 'navigate');
        component.clear();
        expect(spy).toHaveBeenCalledWith([], {
            relativeTo: route,
            queryParams: { season: null },
            queryParamsHandling: 'merge'
        });
    });

    it('should get all competitions', () => {
        const result: CompetitionResultData = new CompetitionResultData();
        result.competitions = [
            new Competition(),
        ];
        const spy = spyOn(competitionService, 'get').and.returnValue(of(result));
        component.searchCompetitions();
        expect(spy).toHaveBeenCalled();
        expect(component.dataSource).toEqual(result.competitions);
    });

    it('should get filtered data competitions', () => {
        const result: CompetitionResultData = new CompetitionResultData();
        result.competitions = [
            new Competition(),
            new Competition(),
        ];
        result.competitions[0].currentSeason = {
            "id": 4,
            "startDate": "2017-08-11T19:00:00Z",
            "endDate": "2018-05-20T16:45:00Z",
            "currentMatchday": 34
        };
        result.competitions[1].currentSeason = {
            "id": 4,
            "startDate": "2018-08-11T19:00:00Z",
            "endDate": "2019-05-20T16:45:00Z",
            "currentMatchday": 34
        };

        //january, 22, 2018
        const startDate = "1516586400000";
        const spy = spyOn(competitionService, 'get').and.returnValue(of(result));
        component.filterCompetitionBySeason(startDate);
        expect(spy).toHaveBeenCalled();
        const expectedResult: any = [result.competitions[1]];
        expectedResult.paginator = undefined;
        expect(component.dataSource).toEqual(expectedResult);
    });

    it('should navigate to team list page success', () => {
        const competition: Competition = new Competition();
        competition.id = 2021;
        let row: any = { value: { row: {} } };
        row.value.row = competition;
        const spy = spyOn(router, 'navigate');
        component.onClick(row);
        expect(spy).toHaveBeenCalledWith([`teams-list/${competition.id}`]);
        expect(spy).toHaveBeenCalledTimes(1);

    });

    it('should not navigate to team list page (fail)', () => {
        const competition: any = new Competition();
        competition.id = null;
        let row: any = { value: { row: {} } };
        row.value.row = competition;
        const spy = spyOn(router, 'navigate');
        component.onClick(row);
        expect(spy).toHaveBeenCalledTimes(0);
    });

});
