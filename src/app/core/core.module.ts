import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoreComponent } from './core.component';
import { HeaderModule } from './header/header.module';
import { SidebarModule } from './sidebar/sidebar.module';

@NgModule({
  imports: [
    CommonModule,
    HeaderModule,
    SidebarModule
  ],
  declarations: [CoreComponent],
  exports: [HeaderModule, SidebarModule]
})
export class CoreModule { }
